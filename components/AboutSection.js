import css from '../scss/AboutSection.module.scss';

const AboutSection = ({ title, children }) => (
    <section className={css.section}>
        <div className={css.sectionTitle}>
            <h2 className={css.sectionTitleText}>
                {title}
            </h2>
        </div>
        <div className={css.sectionContent}>
            {children}
        </div>
    </section>
);

export default AboutSection;
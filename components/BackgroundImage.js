import Image from 'next/image'

const PrismicBackgroundImage = ({
    src,
    alt
}) => (
    <Image className="image--background" src={src} alt={alt} layout="fill" objectFit="cover" />
);

export default PrismicBackgroundImage;
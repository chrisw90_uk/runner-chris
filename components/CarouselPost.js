import { RichText } from "prismic-reactjs";
import Image from 'next/image';

const CarouselPost = ({
    as,
    uid,
    title,
    image,
    date,
    content,
    tags,
    onClick,
}) => {
    const Tag = as;
    const dateToDisplay = new Date(date).toLocaleDateString('en-gb', {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    });
  let img = image;
  if (!Object.keys(img).length) {
    img = {
      url: '/images/blog-placeholder.jpg',
      alt: 'A running track in black in white, overlaid with an orange figure running'
    }
  }
    return (
        <Tag>
          <button className="post" type="button" onClick={() => onClick()}>
            <div className="post__image-container">
                <Image
                    className="post__image image--faded"
                    src={img.url}
                    alt={img.alt}
                    layout="fill"
                    objectFit="cover"
                />
            </div>
            <div className="post__copy">
              <h4 className="post__date">
                {dateToDisplay}
              </h4>
              <h3 className="post__title">{RichText.asText(title)}</h3>
              {tags && tags.length > 0 && (
                <ul className="post__tags">
                  {tags.map(tag => <li key={tag}>{tag}</li>)}
                </ul>
              )}
            </div>
          </button>
          <style jsx global>
              {`
                  .post {
                      background: none;
                      text-align: left;
                      border: none;
                      width: 100%;
                      color: inherit;
                      cursor: pointer;
                      transition: 0.2s;
                      font-family: inherit;
                  }
                  .post:hover .post__image {
                    opacity: 1;
                  }
                  .post__image-container {
                      padding-bottom: 60%;
                      position: relative;
                      margin-bottom: 0.5rem;
                  }
                  .post__image {
                      opacity: 0.5;
                      transition: 0.3s;
                      height: 100%;
                  }
                  .post__title {
                      line-height: 1;
                      text-transform: uppercase;
                      font-size: 1.4rem;
                      color: #3cd03c;
                      z-index: 1;
                  }
                  .post__content {
                      flex: 1 0 0;
                      font-size: 0.8rem;
                      padding-top: 2rem;
                      opacity: 0.8;
                      line-height: 1.4;
                      width: 100%;
                  }
                  .post__tags {
                    padding: 0;
                    margin: 0.2rem 0 0;
                    list-style-type: none;
                  }
                  .post__tags li {
                    padding: 0.4rem;
                    border-radius: 10px;
                    background: #303030;
                    display: inline-block;
                    line-height: 1;
                  }
                  @media all and (max-width: 768px) {
                      .post__image-container {
                          width: 100%;
                          height: auto;
                      }
                      .post__date {
                          position: static;
                      }
                      .post__title {
                          position: static;
                          white-space: initial;
                      }
                      .post__content {
                          padding: 0;
                      }
                  }
            `}
          </style>
        </Tag>
    )
};

export default CarouselPost;
import { useState } from "react";
import css from '../scss/countdown.module.scss';

const Countdown = ({
    className = '',
    countdownPanelClass = '',
    date
}) => {

    const [days, setDays] = useState(null);
    const [hours, setHours] = useState(null);
    const [minutes, setMinutes] = useState(null);
    const [seconds, setSeconds] = useState(null);
    setInterval(function() {
        const now = new Date().getTime();
        const distance = date.getTime() - now;
        setDays(Math.floor(distance / (1000 * 60 * 60 * 24)));
        setHours(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
        setMinutes(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
        setSeconds(Math.floor((distance % (1000 * 60)) / 1000));
    }, 1000);
    return (
        <div className={`${css.countdown} ${className}`.trimRight()}>
            <div className={`${css.countdownUnit} ${countdownPanelClass}`.trimRight()}>
                <span>
                    {days}
                </span>
                <label>
                    D
                </label>
            </div>
            <div className={`${css.countdownUnit} ${countdownPanelClass}`.trimRight()}>
                <span>
                    {hours}
                </span>
                <label>
                    H
                </label>
            </div>
            <div className={`${css.countdownUnit} ${countdownPanelClass}`.trimRight()}>
                <span>
                    {minutes}
                </span>
                <label>
                    M
                </label>
            </div>
            <div className={`${css.countdownUnit} ${countdownPanelClass}`.trimRight()}>
                <span>
                    {seconds}
                </span>
                <label>
                    S
                </label>
            </div>
        </div>
    )
};

export default Countdown;
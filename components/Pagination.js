import Link from 'next/link';
import styles from '../scss/pagination.module.scss';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import LastPageIcon from '@mui/icons-material/LastPage';
import {useRouter} from 'next/router';

const Pagination = ({
  url,
  totalPages,
  page,
}) => {
  const router = useRouter();
  return (
    <div className={styles.pagination}>
      <button
        aria-label="Go to first page"
        className={styles.paginationButton}
        onClick={() => router.push(url + '1')}
        disabled={page === 1}
      >
        <FirstPageIcon />
      </button>
      <button
        aria-label="Go to previous page"
        className={styles.paginationButton}
        onClick={() => router.push(url + (page - 1))}
        disabled={page === 1}
      >
        <NavigateBeforeIcon />
      </button>
      <span className={styles.paginationText}>
        {`Page ${page} of ${totalPages}`}
      </span>
      <button
        aria-label="Go to next page"
        className={styles.paginationButton}
        onClick={() => router.push(url + (page + 1))}
        disabled={page === totalPages}
      >
        <NavigateNextIcon />
      </button>
      <button
        aria-label="Go to last page"
        className={styles.paginationButton}
        onClick={() => router.push(url + totalPages)}
        disabled={page === totalPages}
      >
        <LastPageIcon />
      </button>
    </div>
  )
};

export default Pagination;
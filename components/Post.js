import { RichText } from "prismic-reactjs";
import Image from 'next/image';

const Post = ({
    as,
    uid,
    title,
    image,
    date,
    content,
}) => {
    const Tag = as;
    const dateToDisplay = new Date(date).toLocaleDateString('en-gb', {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    });
    let img = image;
    if (!Object.keys(img).length) {
      img = {
        url: '/images/blog-placeholder.jpg',
        alt: 'A running track in black in white, overlaid with an orange figure running'
      }
    }
    return (
        <Tag className="post">
            <div className="post__image-container">
                <Image
                    className="post__image image--faded"
                    src={img.url}
                    alt={img.alt}
                    layout="fill"
                    objectFit="cover"
                />
            </div>
            <h4 className="post__date">
                {dateToDisplay}
            </h4>
            <h3 className="post__title">{RichText.asText(title)}</h3>
          <div className="post__content">
            {content.map((c, i) => <p key={i}>{c.text}</p>)}
          </div>
            <style jsx global>
                {`
                    .post {
                        
                    }
                    .post__image-container {
                      display: none;
                        padding-bottom: 60%;
                        position: relative;
                        margin-bottom: 0.5rem;
                    }
                    .post__image {
                        opacity: 0.6;
                        height: 100%;
                    }
                    .post__title {
                        line-height: 1;
                        text-transform: uppercase;
                        font-size: 1.4rem;
                        color: #3cd03c;
                        z-index: 1;
                    }
                    .post__date {
                        
                    }
                    .post__content {
                        flex: 1 0 0;
                        font-size: 0.8rem;
                        padding-top: 2rem;
                        opacity: 0.8;
                        line-height: 1.4;
                        width: 100%;
                    }
                    @media all and (max-width: 768px) {
                        .post {
                            flex-direction: column;
                            margin: 1.4rem 0;
                        }
                        .post__image-container {
                            width: 100%;
                            height: auto;
                        }
                        .post__date {
                            position: static;
                        }
                        .post__title {
                            position: static;
                            white-space: initial;
                        }
                        .post__content {
                            padding: 0;
                        }
                    }
              `}
            </style>
        </Tag>
    )
};

export default Post;
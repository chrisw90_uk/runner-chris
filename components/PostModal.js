import Image from 'next/image';
import {PrismicRichText} from '@prismicio/react';
import {useEffect} from 'react';
import closeIcon from '../img/close.png';

const PostModal = ({ isOpen, onClose, post }) => {
  useEffect(() => {
    if (isOpen) {
      window.addEventListener("keydown", onEscapeKeyDown)
    } else {
      window.removeEventListener("keydown", onEscapeKeyDown)
    }
  }, [isOpen]);

  const onEscapeKeyDown = (e) => {
    if (e.key === "Escape") {
      onClose();
    }
  }

  return (
    <>
      <dialog className={`modal ${isOpen ? 'modal--open' : ''}`} open={isOpen}>
        <button className="modal__close-overlay" type="button" onClick={() => onClose()} />
        <div className="modal__content">
          {post && (
            <>
              <div className="modal__header">
                <h2 className="text--green">{post.data.title[0].text}</h2>
                <button onClick={() => onClose()} className="modal__close">
                  <i className="material-icons">close</i>
                </button>
              </div>
              <div className="modal__copy">
                <PrismicRichText field={post.data.content} />
              </div>
            </>
          )}
        </div>
      </dialog>
      <style jsx global>
        {`
        .modal {
          visibility: hidden;
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: rgba(0, 0, 0, 0.8);
          z-index: 100;
          display: flex;
          justify-content: center;
          align-items: center;
          opacity: 0;
          transition: 0.3s;
          padding: 1rem;
        }
        .modal__close {
          background: none;
          border: none;
          color: white;
          position: absolute;
          top: 20px;
          right: 20px;
          padding: 0;
          cursor: pointer;
        }
        .modal__close i {
          font-size: 1.6rem;
          transition: .2s;
        }
        .modal__close:hover i {
          transform: rotate(90deg);
        }
        .modal__close-overlay {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: none;
          border: none;
        }
        .modal--open {
          visibility: visible;
          pointer-events: auto;
          opacity: 1;
          height: 100%;
        }
        .modal__header {
          padding-right: 2rem;
        }
        .modal__content {
          background: #303030;
          padding: 2rem;
          width: 720px;
          transform: translateY(50px);
          transition: 0.3s;
        }
        .modal--open .modal__content {
          transform: translateY(0);
        }
        .modal__copy {
          line-height: 1.4;
          color: white;
          opacity: 0.7;
        }
        .modal__copy *:last-child {
          margin-bottom: 0;
        }
        @media all and (max-width: 768px) {
          .modal__content {
            width: 100%;
            padding: 1.4rem;
          }
        }
        .modal__copy {
          font-size: 0.9rem;
        }
      `}
      </style>
    </>
  )
}

export default PostModal;
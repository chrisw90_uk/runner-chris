import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Post from "./CarouselPost";
import { getPosts } from "../pages/api/posts";

const Posts = ({
    results,
    pageSize,
    totalPages,
}) => {
    const [resultsToShow, setResultsToShow] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [isFetchingPosts, setIsFetchingPosts] = useState(false);
    useEffect(() => {
        setResultsToShow(results);
    }, []);

    const loadNextPage = async () => {
        setIsFetchingPosts(true);
        const {
            data: {
                results,
            }
        } = await axios.get(`/api/posts?pageSize=${pageSize}&page=${currentPage + 1}`);
        setIsFetchingPosts(false);
        setCurrentPage(currentPage + 1);
        setResultsToShow([...resultsToShow, ...results]);
    };

    return (
        <ul className="list--unstyled">
            {results.length > 0 ? (
                <React.Fragment>
                    {resultsToShow.map(post => (
                        <Post
                            key={post.id}
                            as="li"
                            uid={post.uid}
                            title={post.data.title}
                            image={post.data.image}
                            date={post.first_publication_date}
                            content={post.data.content}
                        />
                    ))}
                    {currentPage < totalPages && (
                        <li className="text--center">
                            <button
                                className="btn btn--icon"
                                onClick={() => loadNextPage()}
                                disabled={isFetchingPosts}
                            >
                                {isFetchingPosts ? (
                                    <>
                                        <i className="material-icons rotate">refresh</i>
                                        Loading..
                                    </>
                                ) : (
                                    <>
                                        <i className="material-icons">expand_more</i>
                                        Show more
                                    </>
                                )}
                            </button>
                        </li>
                    )}
                </React.Fragment>
            ) : (
                <li className="text--center">
                    Chris is silent.
                </li>
            )}
        </ul>
    )
};

export default Posts;
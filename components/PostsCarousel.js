import React, { useState, useEffect } from 'react';
import CarouselPost from "./CarouselPost";
import { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import PostModal from './PostModal';

const PostsCarousel = ({
    results,
}) => {
  const [isPostModalOpen, setIsPostModalOpen] = useState(false);
  const [post, setPost] = useState(null);
    return (
        results.length > 0 ? (
            <div className="swiper-container">
                <PostModal
                  isOpen={isPostModalOpen}
                  onClose={() => setIsPostModalOpen(false)}
                  post={post}
                />
                <Swiper
                    modules={[Navigation]}
                    spaceBetween={30}
                    slidesPerView={1}
                    navigation
                    breakpoints={{
                        550: {
                            slidesPerView: 2,
                            spaceBetween: 30,
                        },
                        800: {
                            slidesPerView: 3,
                            spaceBetween: 30,
                        },
                        1100: {
                            slidesPerView: 4,
                            spaceBetween: 30,
                        }
                    }}
                >
                    {results.map(post => (
                        <SwiperSlide key={post.id}>
                            <CarouselPost
                                as="div"
                                uid={post.uid}
                                title={post.data.title}
                                image={post.data.image}
                                date={post.first_publication_date}
                                content={post.data.content}
                                onClick={() => {
                                  setIsPostModalOpen(true);
                                  setPost(post);
                                }}
                            />
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
        ) : (
            <p>Chris is silent.</p>
        )
    )
};

export default PostsCarousel;
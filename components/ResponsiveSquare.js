import css from '../scss/ResponsiveSquare.module.scss';

const ResponsiveSquare = ({ className, children }) => (
  <div className={css.square}>
    <div className={`${css.squareInner} ${className}`}>
      {children}
    </div>
  </div>
);

export default ResponsiveSquare;
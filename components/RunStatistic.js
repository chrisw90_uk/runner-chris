import { RichText } from "prismic-reactjs";

const RunStatistic = ({
    as,
    distance,
    time,
    location,
}) => {
    const Tag = as;
    return (
        <Tag className="run-stat pos--relative">
            <div className="flex flex-align--bottom">
                <h2 className="run-stat__distance">{RichText.asText(distance)}</h2>
                <h3 className="run-stat__time">{RichText.asText(time)}</h3>
            </div>
            <style jsx>
                {`
                    .run-stat {
                        text-transform: uppercase;
                        margin: 1rem 0;
                        opacity: 1;
                    
                        transition: .3s;
                    }
                    .run-stat.visible {
                      opacity: 1;
                      transform: translateX(0);
                    }
                    .run-stat__distance {
                        margin: 0 0.5rem 0 0;
                        font-weight: 100;
                        font-size: 1.6rem;
                        color: #3cd03c;
                        line-height: 1.2;
                    }
                    .run-stat__time {
                        margin: 0;
                        font-size: 2.4rem;
                        line-height: 1;
                    }
                    @media all and (max-width: 1920px) {
                      .run-stat__distance {
                          font-size: 1.4rem;
                      }
                      .run-stat__time {
                          font-size: 2rem;
                      }
                    }
                    @media all and (max-width: 550px) {
                      .run-stat {
                        margin: 0.5rem 0;
                      }
                        .run-stat__distance {
                            font-size: 1rem;
                        }
                        .run-stat__time {
                            font-size: 1.4rem;
                        }
                    }
              `}
            </style>
        </Tag>
    )
};

export default RunStatistic;
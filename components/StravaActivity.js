import numeral from 'numeral';
import polyline from 'google-polyline'
import {formatTime} from '../helpers/time';
const SM = require("@mapbox/sphericalmercator")

const convertPolylineIntoRouteInfo = route => {
    if (!route) return null;
    let points = polyline.decode(route);
    points = points.map(p => convertLatLongToPoint(p));
    const xs = points.map(([x]) => x);
    const ys = points.map(([_, y]) => y);
    const minX = Math.min(...xs);
    const maxX = Math.max(...xs);
    const minY = Math.min(...ys);
    const maxY = Math.max(...ys);
    points = points.reduce((acc, p) => (
        `${acc} ${p.join(',')}`
    ), '');
    return {
        minX,
        maxX,
        minY,
        maxY,
        points
    }
};

const convertLatLongToPoint = latLng => {
    const [lng, lat] = latLng;
    const sm1 = new SM({ size: 256 });
    return sm1.px([lat, lng], 18);
};

const StravaActivity = ({
    as,
    name,
    distance,
    time,
    speed,
    heartRate,
    route,
}) => {
    const Tag = as;
    const polyline = convertPolylineIntoRouteInfo(route);
    return (
        <Tag className="activity">
            <div className="activity__inner">
                <h3>{name}</h3>
                {polyline && (
                    <svg viewBox={`${polyline?.minX} ${polyline?.minY} ${polyline?.maxX - polyline?.minX} ${polyline?.maxY - polyline?.minY}`} xmlns="http://www.w3.org/2000/svg">
                        <polyline points={polyline?.points} fill="none" stroke="white" strokeWidth={polyline?.points.length / 65} />
                    </svg>
                )}
                <ul className="list--unstyled activity__stats flex flex--wrap flex-justify--between">
                    <li className="activity__stats-item">
                        <span>
                            {numeral(distance / 1000).format('0.0')}
                            <sub>km</sub>
                        </span>
                        <label>
                            <i className="material-icons">near_me</i>
                            Distance
                        </label>
                    </li>
                    <li className="activity__stats-item">
                        <span>{formatTime(time)}</span>
                        <label>
                            <i className="material-icons">schedule</i>
                            Time
                        </label>
                    </li>
                </ul>
                <ul className="list--unstyled activity__stats activity__stats-sub flex flex--wrap flex-justify--between">
                    <li className="activity__stats-item">
                        <span>
                            {formatTime(time / (distance / 1000))}
                            <sub>/km</sub>
                        </span>
                        <label>
                            <i className="material-icons">timer</i>
                            Avg. Pace
                        </label>
                    </li>
                    <li className="activity__stats-item">
                        <span>
                            {heartRate ? Math.ceil(heartRate) : '-'}
                            <sub>bpm</sub>
                        </span>
                        <label>
                            <i className="material-icons">favorite</i>
                            Avg. Heartrate
                        </label>
                    </li>
                    <li className="activity__stats-item">
                        <span>
                            {((speed * 60 * 60) / 1000).toFixed(1)}
                            <sub>kph</sub>
                        </span>
                        <label>
                            <i className="material-icons">directions_run</i>
                            Avg. Speed
                        </label>
                    </li>
                </ul>
            </div>
            <style jsx>
                {`
                    h3 {
                        color: #ff8d00;
                    }
                   .activity{
                      padding: 3rem 2rem;
                      position: relative;
                      display: flex;
                      flex-direction: column;
                      background: #262626;
                   }
                   .activity__inner {
                      max-width: 400px;
                      width: 100%;
                      margin: 0 auto;
                   }
                   .activity:last-child {
                      background: #1e1e1e;
                   }
                   .activity__stats {
                        margin-top: auto;
                   }
                   .activity svg {
                        position: absolute;
                        top: 50%;
                        transform: translateY(-50%);
                        right: 5%;
                        max-height: 80%;
                        max-width: 300px;
                        z-index: 1;
                        opacity: 0.08;
                   }
                   .activity__stats-item {
                        margin-bottom: 1rem;
                        position: relative;
                        z-index: 1;
                   }
                   .activity__stats-sub .activity__stats-item {
                        margin-bottom: 0;
                   }
                   .activity__stats-sub .activity__stats-item span {
                        font-size: 1.6rem;
                   }
                   span {
                        font-size: 3.2rem;
                        line-height: 1.3;
                        font-family: 'Source Sans Pro', sans-serif;
                        display: block;
                        font-weight: 600;
                   }
                   i {
                        margin-right: 0.2rem;
                        font-size: 0.8rem;
                   }
                   label {
                        display: flex;
                        align-items: center;
                        font-size: 0.8rem;
                        opacity: 0.6;
                   }
                   @media all and (max-width: 550px) {
                       .activity{
                          padding: 1rem;
                       }
                       span {
                            font-size: 2.6rem;
                       }
                       .activity__stats-sub .activity__stats-item span {
                            font-size: 1.2rem;
                       }
                       label {
                            font-size: 0.7rem;
                       }
                   }
                `}
            </style>
        </Tag>
    )
};

export default StravaActivity;
import Image from 'next/image';
import css from '../scss/world-major.module.scss'
import Countdown from "./Countdown";
import {prismicClient} from '../config';

const WorldMajor = ({
    as,
    name,
    routeImage,
    flagImage,
    completionDate,
    completionTime,
    scheduledDate,
}) => {
    const Tag = as;
    return (
        <Tag className={css.wm}>
            <div className={css.wmContent}>
                {routeImage && (
                  <Image
                    className={css.wmRoute}
                    src={routeImage.url}
                    alt={routeImage.alt}
                    height={150}
                    width={150}
                  />
                )}
                <div className={css.wmContentHeader}>
                    {flagImage && (
                      <Image
                        src={flagImage.url}
                        alt={flagImage.alt}
                        height={25}
                        width={40}
                      />
                    )}
                    <h2 className={css.wmContentTitle}>{name}</h2>
                </div>
            </div>
            <div className={css.wmFooter}>
                {completionTime && (
                    <div className={`${css.wmFooter} ${css.wmCompletion}`}>
                        <i className="material-icons">done</i>
                        <span>{`Completed ${completionDate.getFullYear()} in ${completionTime}`}</span>
                    </div>
                )}
                {scheduledDate && !completionDate && (
                    <div className={`${css.wmFooter} ${css.wmScheduled}`}>
                        <Countdown
                            className={css.wmScheduledCountdown}
                            countdownPanelClass={css.wmScheduledCountdownPanel}
                            date={scheduledDate}
                        />
                    </div>
                )}
            </div>
        </Tag>
    )
};

export default WorldMajor;
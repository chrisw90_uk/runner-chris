import styles from '../../scss/BioSection.module.scss';
import Image from 'next/image';

const BioSection = ({ image, text }) => (
  <section className={styles.bio}>
    <div>
        <div className={styles.bioImage}>
            <Image src={image.url} alt={image.alt} layout="fill" objectFit="cover"/>
        </div>
    </div>
    <div className={styles.bioText}>
      <div className={styles.bioTextWrapper}>
          {text}
      </div>
    </div>
  </section>
)

export default BioSection;
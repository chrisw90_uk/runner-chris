import Image from 'next/image';
import ResponsiveSquare from '../../ResponsiveSquare';
import css from './Gear.module.scss';

const GEAR_IMAGES = {
  "g10380160": require('../../../img/gear/saucony-speed-2.jpg'),
  "g8347390": require('../../../img/gear/saucony-hurricane-23.jpg'),
}

const Gear = ({ id, name, distance }) => (
  <ResponsiveSquare className={css.gear}>
    <div className={css.gearInner}>
      <h3 className={css.gearTitle}>{name}</h3>
      <h4 className={`${css.gearDistance} text--orange`}>{(distance / 1000).toFixed(1)}<sub>km</sub></h4>
    </div>
    <Image
      className={css.gearImage}
      src={GEAR_IMAGES[id]}
      alt={name}
      layout="fill"
      objectFit="cover"
    />
  </ResponsiveSquare>
);

export default Gear;
import ResponsiveSquare from '../../ResponsiveSquare';
import css from './Stat.module.scss';
import {TOTAL_LABELS} from '../../../pages/about';
import {formatTime} from '../../../helpers/time';

const parseValue = (value, type) => {
  switch (type) {
    case 'distance':
      return (
        <>
          {(value / 1000).toFixed(1)}
          <sub>km</sub>
        </>
      );
    case 'time':
      return formatTime(value);
    case 'speed':
      return (
          <>
              {((value * 60 * 60) / 1000).toFixed(1)}
              <sub>km/h</sub>
          </>
      );
    default:
      return value;
  }
}

const Stat = ({ name, value, type, source }) => (
  <ResponsiveSquare className={css.stat}>
    <h3 className={css.statValue}>{parseValue(value, type)}</h3>
    <h4 className={css.statLabel}>{name}</h4>
  </ResponsiveSquare>
);

export default Stat;
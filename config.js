import process from 'process';
import * as prismic from "@prismicio/client";

// Prismic API endpoint
export const prismicApiEndpoint = process.env.PRISMIC_URL;

// Access Token if the repository is not public
// Generate a token in your dashboard and configure it here if your repository is private
export const prismicAccessToken = process.env.PRISMIC_TOKEN;

// Client method to query Prismic

const endpoint = prismic.getRepositoryEndpoint('runner-chris');
export const prismicClient = prismic.createClient(endpoint, { accessToken: prismicAccessToken });

// Number of blog posts per page
export const pageSize = 8;

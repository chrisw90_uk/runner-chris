import {connectToDatabase} from '../utils/mongodb';
import axios from 'axios';

export const getStravaToken = async () => {
  const { db } = await connectToDatabase();
  const {
    access_token,
    refresh_token,
    expires_at,
  } = await db.collection('strava').findOne();
  const now = Date.now() / 1000;
  const isTokenValid = expires_at > now;
  if (!isTokenValid) {
    // refresh token
    const newToken = await axios.post('https://www.strava.com/oauth/token', {
      client_id: process.env.STRAVA_CLIENT_ID,
      client_secret: process.env.STRAVA_CLIENT_SECRET,
      grant_type: 'refresh_token',
      refresh_token,
    });
    await db.collection('strava').findOneAndUpdate({}, {
      $set: {
        "access_token": newToken.data.access_token,
        "refresh_token": newToken.data.refresh_token,
        "expires_at": newToken.data.expires_at,
      }
    });
    return newToken.data.access_token;
  }
  return access_token;
}
import numeral from 'numeral';

export const formatTime = time => {
  let formattedTime = numeral(time).format('00:00');
  formattedTime = formattedTime.split(':');
  if (formattedTime[0] === '0') formattedTime.shift();
  return formattedTime.join(':')
};
import '../scss/global.scss';
import css from '../scss/header.module.scss';
import Link from 'next/link';
import {useRouter} from 'next/router';

const NAV = [
  { title: 'Home', href: '/', route: '/' },
  { title: 'About', href: '/about', route: '/about' },
  { title: 'Blog', href: '/blog/1', route: '/blog/[page]' },
  { title: 'World Majors', href: '/world-majors', route: '/world-majors' },
];

export default function App({ Component, pageProps }) {
    const router = useRouter();
    const { route } = router;
    return (
      <>
          <header className={css.header}>
            <nav>
                {NAV.map(n => (
                  <Link key={n.title} href={n.href}>
                      <a className={`${css.headerNav} ${route === n.route ? css.headerNavActive : ''}`}>
                          {n.title}
                      </a>
                  </Link>
                ))}
            </nav>
          </header>
          <Component {...pageProps} />
      </>
    )
}

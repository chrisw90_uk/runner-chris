import Head from 'next/head';
import {getAthlete} from "./api/strava-gear";
import BioSection from "../components/about/BioSection";
import AboutSection from "../components/AboutSection";
import Gear from '../components/about/Gear/Gear';
import Stat from '../components/about/Stat/Stat';
import {getActivityStats} from "./api/activity";
import {prismicClient} from "../config";
import {RichText} from "prismic-reactjs";

export const STATS = {
    count: {
        source: 'stravaStats',
        label: 'Total Runs',
        type: 'number',
    },
    distance: {
        source: 'stravaStats',
        label: 'Total Distance',
        type: 'distance',
    },
    averageMovingTime: {
        source: 'dbStats',
        label: 'Average Moving Time',
        type: 'time',
    },
    averageDistance: {
        source: 'dbStats',
        label: 'Average Distance',
        type: 'distance',
    },
    averageSpeed: {
        source: 'dbStats',
        label: 'Average Speed',
        type: 'speed',
    },
    total5Ks: {
        source: 'dbStats',
        label: 'Total 5Ks',
        type: 'number',
    },
    total10Ks: {
        source: 'dbStats',
        label: 'Total 10Ks',
        type: 'number',
    },
    totalHalfMarathons: {
        source: 'dbStats',
        label: 'Total Halfs',
        type: 'number',
    },
    totalMarathons: {
        source: 'dbStats',
        label: 'Total Marathons',
        type: 'number',
    },
}

const About = (props) => (
    <>
        <Head>
            <title>About | Chris Runs</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="description" content="Learn all about Chris, an incredibly average runner"/>
            <meta property="og:title" content="About | Chris Runs"/>
            <meta property="og:type" content="website"/>
            <meta property="og:url" content="https://runner.chriswickham.co.uk/about"/>
            <meta property="og:description" content="Learn all about Chris, an incredibly average runner"/>
            <meta property="og:image" content="/images/meta.jpg" />
        </Head>
        <main>
            <div className="wrapper wrapper--xl">
                <div className="page-header">
                    <h1 className="text--green">
                        About
                    </h1>
                </div>
                <article>
                    <BioSection image={props.about.data.image} text={RichText.render(props.about.data.text)} />
                    <AboutSection title="Stats">
                        <ul className="list--unstyled grid grid--4 grid-1400--3 grid-1150--2 grid-550--1 grid-gap--1">
                            {Object.keys(STATS).map((stat) => (
                              <li key={stat}>
                                  <Stat
                                    name={STATS[stat].label}
                                    value={props[STATS[stat].source][stat]}
                                    type={STATS[stat].type}
                                  />
                              </li>
                            ))}
                        </ul>
                    </AboutSection>
                    <AboutSection title="Gear">
                        <ul className="list--unstyled grid grid--3 grid-1150--2 grid-550--1 grid-gap--1">
                            {props.shoes.map(shoe => (
                                <li key={shoe.id}>
                                    <Gear
                                      id={shoe.id}
                                      name={shoe.name}
                                      distance={shoe.distance}
                                    />
                                </li>
                            ))}
                        </ul>
                    </AboutSection>
                </article>
            </div>
        </main>
    </>
);

export const getStaticProps = async (req) => {
    // Strava Data
    const { athlete: { shoes }, stats: { all_run_totals: stravaStats } } = await getAthlete();

    // DB data
    const dbStats = await getActivityStats();

    // Bio data
    const about = await prismicClient.getSingle('about');

    return {
        props: {
            about,
            dbStats,
            stravaStats,
            shoes
        }
    }
}

export default About;
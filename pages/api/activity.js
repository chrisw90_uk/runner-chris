import axios from 'axios';
import nextConnect from 'next-connect';
import { connectToDatabase } from '../../utils/mongodb';

const handler = nextConnect();

const FiveK = 5000;
const TenK = 10000;
const HalfMarathon = 21000;
const Marathon = 42000;

const calculateTotalNumberOfDistanceRuns = (arr, targetDistance) => (
  arr.filter(run => Math.floor(run.distance /1000 ) * 1000 === targetDistance).length
)

export const getActivityStats = async (req, res) => {
  const { db } = await connectToDatabase();
  const response = await db.collection('activities').find({}).toArray();
  const totalDistance = Math.ceil(response.reduce((acc, run) => acc += run.distance || 0, 0));
  const totalMovingTime = response.reduce((acc, run) => acc += run.movingTime || 0, 0);
  return JSON.parse(JSON.stringify({
    averageMovingTime: totalMovingTime / response.length,
    averageDistance: totalDistance / response.length,
    averageSpeed: response.reduce((acc, run) => acc += run.speed || 0, 0) / response.length,
    total5Ks: calculateTotalNumberOfDistanceRuns(response, FiveK),
    total10Ks: calculateTotalNumberOfDistanceRuns(response, TenK),
    totalHalfMarathons: calculateTotalNumberOfDistanceRuns(response, HalfMarathon),
    totalMarathons: calculateTotalNumberOfDistanceRuns(response, Marathon),
  }));
};

handler
  .get(async (req, res) => {
    try {
      const activity = await getActivityStats();
      res.status(200).json(activity);
    } catch (err) {
      res.json(err)
    }
  })

export default handler;
import { prismicClient } from "../../config";
import Prismic from "@prismicio/client";
import nextConnect from 'next-connect';
const handler = nextConnect();

export const getPosts = async (pageSize, page) => {
    const options = { pageSize, page, orderings: '[document.first_publication_date desc]' };
    return await prismicClient.query(
        Prismic.Predicates.at(
            'document.type',
            'blog_post'
        ),
        options,
    );
}

handler.get(async (req, res) => {
    try {
        const { pageSize, page } = req.query;
        const response = await getPosts(pageSize, page);
        res.status(200).json(response);
    } catch (e) {
        res.json(e);
    }
});

export default handler;
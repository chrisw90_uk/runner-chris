import nextConnect from 'next-connect';
import axios from "axios";
import { getStravaToken } from '../../helpers/strava';

const handler = nextConnect();

export const getAthlete = async (req, res) => {
    const accessToken = await getStravaToken();
    const { data: athlete } = await axios.get(`https://www.strava.com/api/v3/athlete`, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        }
    });
    const { data: stats } = await axios.get(`https://www.strava.com/api/v3/athletes/${athlete.id}/stats`, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
        }
    });
    return {
        athlete,
        stats
    }
};

handler
    .get(async (req, res) => {
        try {
            const athelete = await getAthlete();
            res.status(200).json(athelete);
        } catch (err) {
            res.json(err)
        }
    })

export default handler;
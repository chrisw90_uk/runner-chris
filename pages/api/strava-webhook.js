import axios from 'axios';
import nextConnect from 'next-connect';
import { connectToDatabase } from '../../utils/mongodb';
import { getStravaToken } from '../../helpers/strava';

const handler = nextConnect();

handler
    .get(async (req, res) => {
        const VERIFY_TOKEN = "CHRIS_RUNS_STRAVA";
        let mode = req.query['hub.mode'];
        let token = req.query['hub.verify_token'];
        let challenge = req.query['hub.challenge'];
        // Checks if a token and mode is in the query string of the request
        if (mode && token) {
            // Verifies that the mode and token sent are valid
            if (mode === 'subscribe' && token === VERIFY_TOKEN) {
                // Responds with the challenge token from the request
                console.log('webhook verified');
                res.status(200).json({"hub.challenge": challenge});
            } else {
                // Responds with '403 Forbidden' if verify tokens do not match
                res.status(403).end();
            }
        }
    })
    .post(async (req, res) => {
        console.log("webhook event received!", req.query, req.body);
        const { aspect_type, object_type, object_id, updates } = req.body;

        try {
            if (object_type === 'activity') {
                const { db } = await connectToDatabase();
                // Validate token
                const accessToken = await getStravaToken();

                if (aspect_type === 'update') {
                    // Supports update of activity title
                    const { title } = updates;
                    if (title) {
                        await db.collection('activities').updateOne(
                            {
                                remoteId: { $eq: object_id },
                            },
                            {
                                $set: {
                                    "name": title,
                                }
                            },
                            {
                                upsert: true,
                            }
                        )
                    }
                    console.log("activity updated");
                }

                if (aspect_type === 'create') {
                    // Get latest activity
                    const response = await axios.get(`https://www.strava.com/api/v3/activities/${object_id}`, {
                        headers: {
                            Authorization: `Bearer ${accessToken}`,
                        }
                    });
                    const {
                        id,
                        name,
                        start_date,
                        average_cadence,
                        average_speed,
                        average_heartrate,
                        map,
                        distance,
                        moving_time,
                        type,
                    } = response.data;
                    if (type === 'run' || type === 'Run') {
                        await db.collection('activities').updateOne(
                            {
                                remoteId: { $eq: id },
                            },
                            {
                                $set: {
                                    "remoteId": id,
                                    "name": name,
                                    "distance": distance,
                                    "movingTime": moving_time,
                                    "speed": average_speed,
                                    "heartRate": average_heartrate,
                                    "route": map.summary_polyline,
                                    "cadence": average_cadence,
                                    "date": start_date,
                                }
                            },
                            {
                                upsert: true,
                            }
                        )
                        console.log("activity added");
                    }
                }

                if (aspect_type === 'delete') {
                    await db.collection('activities').deleteOne(
                        {
                            remoteId: { $eq: object_id },
                        },
                    )
                    console.log("activity deleted");
                }
            }
            console.log("trigger site build");
            await axios.post('https://api.vercel.com/v1/integrations/deploy/prj_wKbATbMJh4SN2QpxfnoYEqGD4PbN/FfZCCH2diY');
            res.status(200).json({ message: "Webhook ran successfully" });
        } catch (e) {
            console.log(e)
            res.status(500).json(e);
        }
    })

export default handler;
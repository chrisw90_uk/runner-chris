import nextConnect from 'next-connect';
import { connectToDatabase } from '../../utils/mongodb';

const handler = nextConnect();

export const getStravaActivity = async (req, res) => {
    const { db } = await connectToDatabase();
    const activity = await db.collection('activities').find().sort({ "date": -1 }).limit(2).toArray();
    return JSON.parse(JSON.stringify(activity));
};

handler
    .get(async (req, res) => {
        try {
            const activity = await getStravaActivity();
            res.status(200).json(activity);
        } catch (err) {
            res.json(err)
        }
    })

export default handler;
import {prismicClient} from '../../config';
import CarouselPost from '../../components/CarouselPost';
import {useState} from 'react';
import PostModal from '../../components/PostModal';
import Pagination from '../../components/Pagination';
import Head from 'next/head';
import {useRouter} from 'next/router';

const Blog = ({ posts }) => {
  const { total_pages: totalPages, page } = posts;
  const [isPostModalOpen, setIsPostModalOpen] = useState(false);
  const [post, setPost] = useState(null);
  const router = useRouter();
  return (
    <>
      <Head>
        <title>Blog | Chris Runs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Chris: an incredibly average runner"/>
        <meta property="og:title" content="PagePage | Chris Runs"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={`https://runner.chriswickham.co.uk${router.asPath}`}/>
        <meta property="og:description" content="Chris: an incredibly average runner"/>
        <meta property="og:image" content="/images/meta.jpg" />
      </Head>
      <main>
        <div className="wrapper wrapper--xl">
          <div className="page-header">
            <h1>
              <span className="text--green">Blog</span>
            </h1>
          </div>
          <article>
            <PostModal
              isOpen={isPostModalOpen}
              onClose={() => setIsPostModalOpen(false)}
              post={post}
            />
            <ul className="list--unstyled grid grid--6 grid-1400--4 grid-920--3 grid-768--2 grid-420--1 grid-gap--1">
              {posts.results.length === 0 ? (
                <li>No results to show.</li>
              ) : (
                posts.results.map(post => (
                  <CarouselPost
                    key={post.id}
                    as="li"
                    uid={post.uid}
                    title={post.data.title}
                    image={post.data.image}
                    date={post.first_publication_date}
                    content={post.data.content}
                    onClick={() => {
                      setIsPostModalOpen(true);
                      setPost(post);
                    }}
                    tags={post.tags}
                  />
                ))
              )}
            </ul>
            {posts.results.length > 0 && (
              <Pagination page={page} totalPages={totalPages} url="/blog/" />
            )}
          </article>
        </div>
      </main>
    </>
  )
}

export async function getStaticPaths() {
  const posts = await prismicClient.getByType('blog_post', {
    pageSize: 12,
  });
  const { total_pages: totalPages } = posts;
  const paths = Array(totalPages).fill(0).map((_, i) => ({
    params: {
      page: `${i + 1}`,
    }
  }));
  return {
    paths,
    fallback: false
  };
}

export const getStaticProps = async ({ params }) => {
  const { page } = params;
  const posts = await prismicClient.getByType('blog_post', {
    orderings: {
      field: 'document.first_publication_date',
      direction: 'desc',
    },
    pageSize: 12,
    page: page || 1,
  });
  return {
    props: {
      posts
    }
  }
}

export default Blog;
import { useEffect, useRef } from "react";
import { getStravaActivity } from "./api/strava";
import { RichText } from 'prismic-reactjs';
import Head from 'next/head'
import Image from 'next/image'
import RunStatistic from "../components/RunStatistic";
import StravaActivity from "../components/StravaActivity";
import Countdown from "../components/Countdown";
import PostsCarousel from "../components/PostsCarousel";
import {prismicClient} from "../config";
import * as prismic from "@prismicio/client"

const Home = ({ stravaData, doc: { data }, postsByTag, nextRace }) => {
    const { body: distances } = data;
    const title = useRef(null);
    const subtitle = useRef(null);
    useEffect(() => {
        const raceTimes = document.getElementsByClassName("run-stat");
        title.current.classList.add('visible');
        subtitle?.current?.classList.add('visible');
        for (let i = 0; i < raceTimes.length; i++) {
            setTimeout(() => {
                raceTimes[i].classList.add('visible');
            }, (i + 1) * 200)
        }
    })
    let titleParts = data.home_title[0].text.split(' ');
    return (
        <div className="container">
            <Head>
                <title>Chris Runs</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="description" content="Chris: an incredibly average runner"/>
                <meta property="og:title" content="Chris Runs"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content="https://runner.chriswickham.co.uk"/>
                <meta property="og:description" content="Chris: an incredibly average runner"/>
                <meta property="og:image" content="/images/meta.jpg" />
            </Head>
            <main>
                <section className="hero hero--full-page hero--fade flex flex--column flex-justify--center">
                    <Image
                        layout="fill"
                        objectFit="cover"
                        className="hero__image"
                        src="/images/background.jpg"
                        alt="Chris running the London Marathon"
                    />
                    <div className="hero__content text--left">
                        <div />
                        <div>
                            <h1 className="hero__content-title" ref={title}>
                                <span className="text--green">{titleParts[0]}</span>
                                &nbsp;
                                {titleParts[1]}
                            </h1>
                            {nextRace ? (
                              <div>
                                  <h2>
                                      <span className="text--green text--uppercase">Next race</span> {RichText.asText(nextRace.data.title)}
                                  </h2>
                                  <div className="hero__countdown">
                                      <Countdown
                                        date={new Date(nextRace.data.date)}
                                        countdownPanelClass="hero__countdown-panel"
                                      />
                                  </div>
                              </div>
                            ) : (
                              <h2 className="hero__content-title" ref={subtitle}>
                                  {data.home_subtitle[0].text}
                              </h2>
                            )}
                        </div>
                        <ul className="list--unstyled">
                            {distances.map((d, i) => (
                                <RunStatistic
                                    key={i}
                                    as="li"
                                    distance={d.primary.distance_type}
                                    time={d.primary.distance_time}
                                    location={d.primary.distance_location}
                                />
                            ))}
                        </ul>
                    </div>
                </section>
                <section className="section">
                    <div className="section__header">
                        <h2 className="section__header-title">
                            Activities
                        </h2>
                        <div className="margin-top--xs flex flex-align--center flex-justify--center">
                            <small className="margin-r--xs">Powered by</small>
                            <Image
                                height={22}
                                width={100}
                                src="/images/strava-logo.png"
                                alt="Strava Logo"
                            />
                        </div>
                    </div>
                    <ul className="list--unstyled grid grid--2 grid-920--1">
                        {stravaData && stravaData.map(run => (
                            <StravaActivity
                                as="li"
                                key={run.remoteId}
                                name={run.name}
                                distance={run.distance}
                                time={run.movingTime}
                                speed={run.speed}
                                heartRate={run.heartRate}
                                route={run.route}
                                cadence={run.cadence}
                            />
                        ))}
                    </ul>
                </section>
                <section className="section">
                    <div className="section__header">
                        <h2 className="section__header-title">
                            Blogs
                        </h2>
                    </div>
                    <div className="wrapper">
                        {Object.keys(postsByTag).map(tag => (
                          <div className="margin-btm--md">
                            <h3 className="section__header-subtitle">{tag}</h3>
                            <PostsCarousel results={postsByTag[tag]} />
                          </div>
                        ))}
                    </div>
                </section>
            </main>
        </div>
    )
};

export const getStaticProps = async (req) => {
    const doc = await prismicClient.getSingle('home');
    const tags = await prismicClient.getTags();

    // Next Race
    let dateString = new Date().toISOString();
    dateString = dateString.slice(0, -5) + 'Z';
    const nextRace = await prismicClient.getByType('race', {
        orderings: {
            field: 'document.date',
            direction: 'desc',
        },
        predicates: [prismic.predicate.dateAfter('my.race.date', dateString)],
        pageSize: 1
    });

    // Posts by Tag
    tags.reverse();
    const promises = [];
    tags.forEach(tag => {
        promises.push(prismicClient.getAllByTag(tag, {
            orderings: {
                field: 'document.first_publication_date',
                direction: 'desc',
            },
            pageSize: 8,
            page: 1,
        }))
    })
    const postsByTag = await Promise.all(promises).then((res) => {
        return tags.reduce((acc, tag, i) => ({
            ...acc,
            [tag]: res[i],
        }), {});
    })
    // Strava
    const stravaData = await getStravaActivity();
    return {
        props: {
            doc,
            nextRace: nextRace.results[0] || null,
            stravaData,
            postsByTag,
        },
    }
};

export default Home;

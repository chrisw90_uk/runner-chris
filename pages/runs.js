import {pageSize, prismicClient} from '../config';
import Prismic from '@prismicio/client';
import { getActivityStats } from './api/activity';

const Runs = ({
  data
}) => {
  console.log(data);
  return (
    <p>Hello</p>
  )
};

export const getStaticProps = async (req) => {
  const data = await getActivityStats();
  return {
    props: {
      data,
    },
  }
};

export default Runs;
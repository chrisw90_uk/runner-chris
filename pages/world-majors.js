import WorldMajor from "../components/WorldMajor";
import {prismicClient} from '../config';
import Head from 'next/head';

const WorldMajors = ({ races }) => (
  <>
    <Head>
      <title>World Majors | Chris Runs</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="Chris, an incredibly average runner, attempts to run all six world majors"/>
      <meta property="og:title" content="World Majors | Chris Runs"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="https://runner.chriswickham.co.uk/world-majors"/>
      <meta property="og:description" content="Chris, an incredibly average runner, attempts to run all six world majors"/>
      <meta property="og:image" content="/images/meta.jpg" />
    </Head>
    <main>
      <div className="wrapper wrapper--xl">
        <div className="page-header">
          <h1>
            <span className="text--green">World Majors</span> Series
          </h1>
        </div>
        <article>
          <ul className="list--unstyled grid grid--3 grid-1280--2 grid-768--1 grid-gap--2 grid-550-gap--1">
            {races.map(race => (
              <WorldMajor
                key={race.id}
                as="li"
                name={race.data.name[0].text}
                routeImage={race.data.route}
                flagImage={race.data.country_flag}
                completionDate={race.data.completed ? new Date(race.data.completed) : null}
                completionTime={race.data.time[0]?.text}
                scheduledDate={race.data.scheduled ? new Date(race.data.scheduled) : null}
              />
            ))}
          </ul>
        </article>
      </div>
    </main>
  </>
)

export const getStaticProps = async (req) => {
  const { results: races } = await prismicClient.getByType('world_major', {
    orderings: {
      field: 'my.world_major.scheduled',
      direction: 'asc',
    },
  });
  races.sort((a, b) => {
    if (!a.data.scheduled || !b.data.scheduled) return -1
  });
  return {
    props: {
      races
    }
  }
}

export default WorldMajors;
import { MongoClient } from 'mongodb'
const { DB_CONNECTION } = process.env;

if (!DB_CONNECTION) {
    throw new Error(
        'Please define the DB_CONNECTION environment variable'
    )
}

/**
 * Global is used here to maintain a cached connection across hot reloads
 * in development. This prevents connections growing exponentially
 * during API Route usage.
 */
let cached = global.mongo;

if (!cached) {
    cached = global.mongo = { conn: null, promise: null }
}

export async function connectToDatabase() {
    if (cached.conn) {
        return cached.conn
    }

    if (!cached.promise) {
        const opts = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        };

        cached.promise = MongoClient.connect(DB_CONNECTION, opts).then((client) => {
            return {
                client,
                db: client.db('runnerChris'),
            }
        })
    }
    cached.conn = await cached.promise;
    return cached.conn
}